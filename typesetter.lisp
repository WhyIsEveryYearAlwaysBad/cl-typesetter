(defvar *format-mode* 'plain "The type of string to write.")
 
(defun bold (text)
  (ccase *format-mode*
    (plain text)
    (ansi-terminal (concatenate 'string "[1m" text "[0m"))
    (markdown (concatenate 'string "**" text "**"))
    )
  )

(defun italic (text)
  (ccase *format-mode*
    (plain text)
    (ansi-terminal (concatenate 'string "[3m" text "[0m"))
    (markdown (concatenate 'string "_" text "_"))
    )
  )

(defun underline (text)
  (ccase *format-mode*
    (plain text)
    (ansi-terminal (concatenate 'string "[4m" text "[0m"))
    )
  )

(defun slow-blink (text)
  "Formats the text with the slow blinking ANSI terminal code."
  (ccase *format-mode*
    (plain text)
    (ansi-terminal (concatenate 'string "[5m" text "[0m"))
    )
  )

(defun fast-blink (text)
  "Formats the text with the slow blinking ANSI terminal code."
  (ccase *format-mode*
    (plain text)
    (ansi-terminal (concatenate 'string "[6m" text "[0m"))
    )
  )

;; Simple text functions for literacy.
(defun sentence (&rest texts)
  "Produces text as it's a sentence. Appends a space after each text."
  ;; (concatenate 'string (apply #'concatenate 'string texts) (list #\Space))
  (apply #'concatenate 'string
	       (do*
		(
		 (text-iterator texts (cdr text-iterator))
		 (spaced-string (concatenate 'string (car text-iterator) (list #\Space)) (concatenate 'string (car text-iterator) (list #\Space)))
		 (new-list (list spaced-string) (nconc new-list (list spaced-string)))
		 )
		((null (cdr text-iterator)) new-list)
		 
		)
	       )
  )

(defun paragraph (&rest texts)
      "Marks a paragraph. Appends 2 newlines to the end."
  (concatenate 'string (apply #'concatenate '(string) texts) (list #\Newline #\Newline))
  )

;; Higher-level functions for document structure 
(defun document-title (text)
  "Writes a title for a document."
  (ccase *format-mode*
    (plain (format nil "~35T~a~35T~&" text))
    (ansi-terminal (format nil "~35T~a~35T~&" (bold text)))
    (markdown (header 1 text))
     )
    )

(defun header (level text)
  "Creates a header."
  (ccase *format-mode*
    ((plain ansi-terminal)
       (concatenate 'string text
		    (list #\Newline)
		    (make-sequence 'string (length text) :initial-element
								 (case level (1 #\-)
								       (2 #\=)
								       (3 #\-)
								       (4 #\=))) (list #\Newline)))
    (markdown
       (concatenate 'string
		    (make-sequence 'list level :initial-element #\#)
		    (list #\Space)
		    text
		    (list #\Newline))))
  )

(defun resource-locator (text &optional display-text)
  "Formats text as a link to a resource."
  (ccase *format-mode*
    (plain (concatenate 'string
			(if (null display-text)
			  ()
			  (concatenate 'string display-text (list #\Space))) (list #\<) text (list #\>)))
    (ANSI-terminal (underline (concatenate 'string "[94m<" text ">")))
    (markdown (cond
		((null display-text) (concatenate 'string (list #\<) text (list #\>)))
		(t (concatenate 'string "[" display-text "](" text ")"))
		))
     )
  )

;; Function to write a card Zettelkasten.
(defun zettelkasten-card (data link tags &optional (referred-links nil))
  "Writes a Zettelkasten.."
  (ccase *format-mode*
    (plain (format nil "~0,36<Link: ~a~1,36;Tags: ~a~2&~a~>~&~0,72<Zettelkasten~;~a~>~&"  link tags data referred-links))
    (ansi-terminal (format nil "~0,35<~a~1,35;~a~>~2&~a~2&Zettelkasten~35T[94m~a[0m~&"  link tags data
			   (map 'list #'resource-locator referred-links))
     )
    )
  )
   
